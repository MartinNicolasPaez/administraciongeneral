﻿using AdministracionGeneral.Apis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdministracionGeneral.Controllers
{
    public class BusquedasDeClientesController : BaseController
    {
        // GET: BusquedasDeClientes
        public ActionResult Index()
        {
            List<AdministracionGeneral.Models.busquedaCliente_log> listado = new List<Models.busquedaCliente_log>();

            listado = new busquedaCliente().Get();


            foreach (var item in listado)
            {

                try
                {

                var jsonParsiado = JObject.Parse(item.query);
                JToken hits = jsonParsiado["query"].SelectToken("bool").SelectToken("must").FirstOrDefault().SelectToken("query_string").SelectToken("query");
                item.query = hits.ToString();
                }
                catch (Exception)
                {
                    item.query = "sin busqueda";
                }



                

            }





            return View(listado);
        }



        
        public JsonResult Delete(int id)
        {
            bool rta;

            List<AdministracionGeneral.Models.busquedaCliente_log> listado = new List<Models.busquedaCliente_log>();

            rta= new busquedaCliente().Delete(id);


            return Json(new {rta},JsonRequestBehavior.AllowGet);
        }

        // GET: BusquedasDeClientes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BusquedasDeClientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BusquedasDeClientes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BusquedasDeClientes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BusquedasDeClientes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        
    }
}
