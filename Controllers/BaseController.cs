﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using AdministracionGeneral.Models;



namespace AdministracionGeneral.Controllers
{   
    public class BaseController: Controller
    {
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            var session = Session["nombre"];

            string areaName = filterContext.RequestContext.RouteData.DataTokens.ContainsKey("area") ? filterContext.RequestContext.RouteData.DataTokens["area"].ToString() : null;

            string controllerName = filterContext.RequestContext.RouteData.Values["controller"].ToString();

            string actionName = filterContext.RequestContext.RouteData.Values["action"].ToString();

            if (!(areaName == null && controllerName == "Account" && (actionName == "Login" || actionName == "LogOff")))
            {
                if (session == null)
                {
                    filterContext.Result = RedirectToAction("index", "Home");
                    return;
                }

            }


            base.OnAuthorization(filterContext);
        }


    }
}