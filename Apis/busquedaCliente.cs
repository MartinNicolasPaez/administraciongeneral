﻿using AdministracionGeneral.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdministracionGeneral.Apis
{
    public class busquedaCliente : ApiController
    {
        public List<AdministracionGeneral.Models.busquedaCliente_log> Get()
        {
            gestionEntities context = new gestionEntities();

            var lista = context.busquedaCliente_log.ToList();
            return lista;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }
                
        public bool Delete(int id)
        {
            try
            {
                gestionEntities context = new gestionEntities();
                var registro = context.busquedaCliente_log.FirstOrDefault(X => X.id == id);
                context.busquedaCliente_log.Remove(registro);
                context.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }

            
            
        }
    }
}