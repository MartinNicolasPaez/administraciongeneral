﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdministracionGeneral.Startup))]
namespace AdministracionGeneral
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
